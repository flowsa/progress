<?php
/**
 * progress plugin for Craft CMS 3.x
 *
 * For Vinpro project
 *
 * @link      https://www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communications
 */

/**
 * @author    Flow Communications
 * @package   Progress
 * @since     1.0.0
 */
return [
    'progress plugin loaded' => 'progress plugin loaded',
];
