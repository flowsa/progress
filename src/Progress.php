<?php
/**
 * progress plugin for Craft CMS 3.x
 *
 * For Vinpro project
 *
 * @link      https://www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communications
 */

namespace flowsa\progress;

use flowsa\progress\services\ProgressService as ProgressServiceService;
use flowsa\progress\variables\ProgressVariable;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\twig\variables\CraftVariable;

use yii\base\Event;

/**
 * Class Progress
 *
 * @author    Flow Communications
 * @package   Progress
 * @since     1.0.0
 *
 * @property  ProgressServiceService $progressService
 */
class Progress extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var Progress
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('progress', ProgressVariable::class);
            }
        );

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                }
            }
        );

        Craft::info(
            Craft::t(
                'progress',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

}
