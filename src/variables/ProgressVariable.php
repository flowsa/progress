<?php
/**
 * progress plugin for Craft CMS 3.x
 *
 * For Vinpro project
 *
 * @link      https://www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communications
 */

namespace flowsa\progress\variables;

use flowsa\progress\Progress;

use Craft;

/**
 * @author    Flow Communications
 * @package   Progress
 * @since     1.0.0
 */
class ProgressVariable
{
    // Public Methods
    // =========================================================================

    /**
     * @param $user
     * @param $entry
     * 
     * @return string
     */
    public function progressCount($user, $entry)
    {
        $result = Progress::$plugin->progressService->countService($user, $entry);
        return $result;
    }

    /**
     * @param $user
     * @param $asset
     * 
     * @return boolean
     */
    public function downloadCheck($user, $asset)
    {
        $result = Progress::$plugin->progressService->downloadCheckService($user, $asset);
        return $result;
    }
}
