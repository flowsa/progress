<?php
/**
 * progress plugin for Craft CMS 3.x
 *
 * For Vinpro project
 *
 * @link      https://www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communications
 */

namespace flowsa\progress\services;

use flowsa\progress\Progress;

use Craft;
use craft\base\Component;

/**
 * @author    Flow Communications
 * @package   Progress
 * @since     1.0.0
 */
class ProgressService extends Component
{
    // Public Methods
    // =========================================================================

    /*
     * @return mixed
     */
    public function countService($user, $entry)
    {

        $downloadIds = [];
        foreach($entry->relatedDownloads as $download){
            $downloadIds[] = $download->id;
        }

        $userDownloads = (new \craft\db\Query()) 
        ->select(['assetId','userId']) 
        ->from('craft_linkvault_downloads') 
        ->where(['userId' => $user->id])
        ->andWhere(['in', 'assetId',$downloadIds])
        ->all();

        $downloadedIds = [];
        foreach($userDownloads as $userDownload){
            $downloadedIds[] = $userDownload['assetId'];
        }
        if(count($downloadIds)){
            $percentage = (count(array_unique($downloadedIds)) / count($downloadIds)) * 100;
        } else{
            $percentage = 0;
        }
        return $percentage;
    }

        /*
     * @return mixed
     */
    public function downloadCheckService($user, $asset)
    {
        //print_r($asset)
        $userDownloads = (new \craft\db\Query()) 
        ->select(['assetId','userId']) 
        ->from('craft_linkvault_downloads') 
        ->where(['userId' => $user->id])
        ->andWhere(['assetId'=> $asset->id])
        ->all();

        if($userDownloads ){
            return true;
        }
        return false;
    }
}
